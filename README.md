# ls_reservations_limited_places

# FR
un script pour limiter la possibilité de s'inscrire à des atelier en fonction du nombre de places disponibles et de places prises

## pré-requis
- php 7.3
- limesurvey 3.21.6

## instructions
- adapter connect.ls.pdo.inc.php avec vos valeurs de connexion MySQL pour votre LS et l'url de votre limesurvey
- uploader le contenu de survey_archive_576573.lsa (il contient aussi les réponses)
- adapter le script 576573.php; il doit avoir le même nom que le SID de votre formulaire LS
- adapter les variables JS dans la 1e question, cachées dans l'aide (afficher le code source), en reprenant les valeurs fournies par LS (c'est le boulot le plus délicat)
- adapter les champs cachés avec vos valeurs limites: elles sont indiquées, en fin de ligne, avec en séparateur le caractère #

Exemple:
      atelier 1: sauce tomate: #1
      atelier 2: soljanka, potage moscovite: #2
      atelier 3: sacher-torte: #3

Dans cet exemple, le 1er atelier a une place, le 2e 2 et le 3e atelier 3

## fonctionnement
- par défaut, les items de réponse de la question "atelier" sont cachés dans LS
- lorsque l'url de script php est appelé, le script se connecte à LS, extrait les résultats du LS avec l'ID matchant le nom du script; il extrait des champs cachés la valeurs limites); en faisant un loop sur les items de réponses de la question nommé "atelier", il vérifie pour chaque valeur le nombre de réservations; si le nombre est atteint, il affiche l'item en grisé, sinon il créé un lien sur le LS avec l'item en variable
- lorsque le LS est appellé, un affreux JS montre l'item caché en récupérant son identifiant; en gros, il commence par charger une 1e fonction JS depuis le code-source de "groupe", puis un script JS dans le code-source de la question "aletier" qui affice l'item retenu dans le script php, en laissant les autres cachés

## attention
le script calcule sur les valeurs fournies par la base de données des résultats du formulaire; comme on a souvent des personnes qui s'inscrivent plus d'une fois, il faut régulièrement *vérifier vos données et supprimer les doublons* afin que le calcul soit correct

------

# en
use limesurvey (LS) and a script to limit reservations for an event (usefull  in covid-19 times!)

## instructions

-  adapt connect.ls.pdo.inc.php with your MySQL connection values for your LS and the url of your limesurvey
-  upload the content of survey_archive_576573.lsa (it also contains the answers)
-  adapt script 576573.php; it must have the same name as the SID of your LS form
-  adapt the JS variables in the 1st question, hidden in the help (display the source code), by taking the values provided by LS (this is the most delicate job)
-  adapt the hidden fields with your limit values: they are indicated at the end of the line with the # character as separator.

Example:
- atelier 1: tomato sauce: #1
- atelier 2: soljanka, russian soup: #2
- workshop 3: sacher-torte (a nice austrian dessert): #3

In this example, the 1st atelier (workshop) has one place, the 2nd 2 places and the 3rd workshop 3 places.

## operation

-  by default, the answer items of the "workshop" question are hidden in LS
-  when the php script url is called, the script connects to LS, extracts the results from LS with the ID matching the script name; it extracts from the hidden fields the limit values); by looping on the answer items of the question named "workshop", it checks for each value the number of reservations; if the number is reached, it displays the item in gray, otherwise it creates a link on the LS with the item as a variable
-  when the LS is called, an ugly JS shows the hidden item by retrieving its identifier; basically, it starts by loading a 1st JS function from the "group" source code, then a JS script in the source code of the "aletier" question that matches the item selected in the php script, leaving the others hidden

## caution!
the script calculates on the values provided by the database of the form results; as we often have people who register more than once, it is necessary to regularly check your data and remove duplicates so that the calculation is correct

Translated with www.DeepL.com/Translator (free version)
