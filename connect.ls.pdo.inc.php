<?php
//adapt with your LS mysql connexion values
$dsn = 'mysql:dbname=LSDBNAME;host=localhost;port=3306';
$user = 'LSUSER';
$password = 'LSPASSWD';
$url_limesurvey="https://localhost/outils/limebooking3/index.php/"; //adapt with your LS url

		try {
		    $db = new PDO($dsn, $user, $password);
	  		$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		} catch (PDOException $e) {
		    echo 'Connection failed: ' . $e->getMessage();
		}

?>
